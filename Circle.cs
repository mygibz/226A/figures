﻿using System;

namespace Figures {
    class Circle : Figure {
        double _radius;

        public double radius {
            get => _radius;
            set {
                if (value >= 0.0) {
                    _radius = value;
                }
            }
        }

        public Circle(double xPosition = 100, double yPosition = 100, double radius = 75) : base(xPosition, yPosition) {
            _radius = radius;
            Console.WriteLine("Circle was Drawn");
        }
        public new void GetPosition() {
            Console.WriteLine($"The circle is located at {_xPosition + (radius/2)}/{_yPosition - (radius / 2)}");
        }
    }
}
