﻿using System;

namespace Figures {
    class Figure {
        protected double _xPosition = 100;
        protected double _yPosition = 100;

        public Figure(double x, double y) {
            _xPosition = x;
            _yPosition = y;
        }

        public Figure() { }

        public void GetPosition() {
            Console.WriteLine(@$"{_xPosition}/{_yPosition}");
        }

        public virtual void Draw() {

        }
    }
}
