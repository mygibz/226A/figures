# Zeichnungsprogramm

Sie sollen eine Applikation zum Zeichnen einfacher geometrischer Figuren, wie beispielsweise Kreis, Reckteck oder Dreieck erstellen. Als gute Programmiererin bzw. guter Programmierer erkennen Sie natürlich sofort, dass Sie hier das Konzept der Generalisierung anwenden können.

## Hinweis

In der Programmierung ist es üblich, dass der Ursprung des Koordinatensystems (0,0) sich in der oberen, linken Ecke des Bildschirms bzw. des Fensters befindet. Die x-Achse erstreckt sich dabei von links nach rechts (analog zur x-Achse in der Mathematik). Anders als traditionelle Koordinatensysteme in der Mathematik, wächst die y-Achse von oben nach unten. Der höchste y-Wert befindet sich folglich am unteren Ende des Bildschirms bzw. des Fensters.

## Aufgabe

Erstellen Sie ein neues C# Konsolen-Projekt mit den drei Klassen `Figure`, `Circle` und `Rectangle`. Achten Sie dabei auf das objektorientierte Konzept der Kapselung und beachten Sie die folgenden Anforderungen:

1. Damit Sie alle Figuren auf der Zeichenfläche platzieren können, sollen diese eine X- und Y-Koordinate (`double`) haben.
2. Werte für diese beiden Koordinaten können, müssen jedoch nicht zwingend, beim Erzeugen der jeweiligen Objekte angegeben werden.
3. Wenn keine Werte für die X- und Y-Koordinate angegeben werden, wird jede Figur an der Position [100, 100] gezeichnet.
4. Die spezifischen Merkmale von Kreis und Rechteck sollen ebenfalls beim Erzeugen von Objekten der jeweiligen Klasse angegeben werden *können* (nicht zwingend).
5. Bei der Zuweisung von Werten zu figurspezifischen Merkmalen sollen nur positive Werte angenommen werden.
6. Durch den Aufruf einer Methode mit dem Namen `GetPosition` soll die Position (obere, linke Ecke) anhand der Koordinaten auf die Konsole geschrieben werden.