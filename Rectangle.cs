﻿using System;

namespace Figures {
    class Rectangle : Figure {
        double _width, _height;
        public double width {
            get => _width;
            set {
                if (value >= 0.0) {
                    _width = value;
                }
            }
        }
        public double height {
            get => _height;
            set {
                if (value >= 0.0) {
                    _height = value;
                }
            }
        }

        public Rectangle(double xPosition = 100, double yPosition = 100, double width = 70, double height = 120) : base(xPosition, yPosition) {
            _width = width;
            _height = height;
            Console.WriteLine("Rectangle was Drawn");
        }

        public void GetSize() {
            Console.WriteLine($@"{_width}x{_height}");
        }

        public void Rotate90DegreesClockwise() {
            double tmp = _height;
            _height = _width;
            _width = tmp;

            _xPosition += _width;
            _yPosition = _yPosition + _height -_width;
        }

        public new void GetPosition() {
            //base.GetPosition();
            Console.WriteLine($"The rectangle is located at {_xPosition}/{_yPosition} (top-left) and {_xPosition + _width}/{_yPosition - _height} (bottom right)");
        }
    }
}
